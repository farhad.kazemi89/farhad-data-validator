let ajvCompiles = {};

function setCompiledSchema(ajvCompile) {
    ajvCompiles = ajvCompile;
    // console.log(ajvCompiles);
}


//@ desc - this class checks validation of req.data with already compiled schemas
//@ param - String - schema name in the config of schema-compile module
class DataValidatorMiddleware {
    constructor(schemaName) {
        let validate = ajvCompiles[schemaName];
        return (req, res, next) => {
            try {
                const valid = validate(req.data);
                // console.log(`req.data : ${req}, schemaName : ${schemaName}`);
                if (valid){
                    next();
                }
                else{
                    console.log(`error 400 bad request: ${validate.errors[0].schemaPath}`);
                    throw new Error(validate.errors[0].schemaPath);
                }
            }catch(err){
                return res.writeHead(400,{ 'Content-Type' : 'application/json'}).end(JSON.stringify(`400 Bad Request, Data Validation Error: ${err}`));
                // throw new Error(`error: ${err}`);
            }
        }
    }
}


module.exports = {
    DataValidatorMiddleware,
    setCompiledSchema
}